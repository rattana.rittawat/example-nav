@file:Suppress("NOTHING_TO_INLINE")
package com.taywin.satfpayslip.util

import timber.log.Timber

//
// Static methods on the Timber object
//

/** Invokes an action if any trees are planted */
/*
inline fun ifPlanted(action: () -> Unit) {
    if (BuildConfig.DEBUG) {
        if (Timber.treeCount() != 0) {
            action()
        }
    }
}

*/
/** Delegates the provided message to [Timber.e] if any trees are planted. *//*

inline fun e(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.e(message().toString())
}

*/
/** Delegates the provided message to [Timber.e] if any trees are planted. *//*

inline fun e(throwable: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.e(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.w] if any trees are planted. *//*

inline fun w(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.w(message().toString())
}

*/
/** Delegates the provided message to [Timber.w] if any trees are planted. *//*

inline fun w(throwable: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.w(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.i] if any trees are planted. *//*

inline fun i(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.i(message().toString())
}

*/
/** Delegates the provided message to [Timber.i] if any trees are planted. *//*

// inline fun i(throwable: Throwable, message: () -> String?) = ifPlanted {
//    if (message() != null)
//        Timber.i(throwable, message().toString())
// }

inline fun i(throwable: Throwable, message: () -> String?) {
    if (BuildConfig.DEBUG)
        Timber.i(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.d] if any trees are planted. *//*

inline fun d(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.d(message().toString())
}

*/
/** Delegates the provided message to [Timber.d] if any trees are planted. *//*

inline fun d(throwable: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.d(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.v] if any trees are planted. *//*

inline fun v(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.v(message().toString())
}

*/
/** Delegates the provided message to [Timber.v] if any trees are planted. *//*

inline fun v(throwable: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.v(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.wtf] if any trees are planted. *//*

inline fun wtf(message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.wtf(message().toString())
}

*/
/** Delegates the provided message to [Timber.wtf] if any trees are planted. *//*

inline fun wtf(throwable: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.wtf(throwable, message().toString())
}

*/
/** Delegates the provided message to [Timber.log] if any trees are planted. *//*

inline fun log(priority: Int, t: Throwable, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.log(priority, t, message().toString())
}

*/
/** Delegates the provided message to [Timber.log] if any trees are planted. *//*

inline fun log(priority: Int, message: () -> String?) = ifPlanted {
    if (BuildConfig.DEBUG)
        Timber.log(priority, message().toString())
}*/
