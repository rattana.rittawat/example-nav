package com.taywin.satfpayslip.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.annotation.DrawableRes
import com.taywin.satfpayslip.R
import com.taywin.satfpayslip.databinding.DialogDateTimerBinding

class AlertDialogUtils {
    companion object {
        private var dialog: Dialog? = null
        private const val DURATION: Long = 350

        interface OnDialogStatusListener {

            fun onPositiveButtonClick(tag: String)
        }

        interface OnDialogConfirmListener {

            fun onConfirmButtonClick(tag: String)

            fun onDismissButtonClick(tag: String)
        }

        interface OnDialogConfirmGameListener {

            fun onConfirmButtonClick(tag: String)

            fun onTop10ButtonClick(tag: String)

            fun onDismissButtonClick(tag: String)
        }
    }

    class ShowDialogDateTimer(
            context: Context,
            listener: OnDialogConfirmGameListener?,
            title: String,
            msg: String,
            btnMsg: String,
            @DrawableRes imgRes: Int? = R.drawable.ic_arrow_down,
            alpha: Float = 0.5f,
            tag: String = ""
    ) :
            Dialog(context) {

        init {
            dialog = this
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val binding =
                    DialogDateTimerBinding.inflate(LayoutInflater.from(context), null, false)
            dialog?.setContentView(binding.root)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog?.window?.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            dialog?.window?.attributes = lp
            binding.tvConfirm.setOnClickListener {
                listener?.onConfirmButtonClick(tag)
                dialog?.dismiss()
            }
            binding.tvCancel.setOnClickListener {
                dialog?.dismiss()
            }
            dialog?.show()
        }
    }



}