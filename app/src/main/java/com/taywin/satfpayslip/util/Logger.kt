package com.taywin.satfpayslip.util

import timber.log.Timber

class Logger private constructor(clazz: Class<*>) {

   /* private val tag: String = clazz.simpleName

    fun debug(msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                d { "$msg" }
            }
        }
    }

    fun debug(tag: String, msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).d("$msg")
            }
        }
    }

    fun debug(msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).d(tr, "$msg")
            } else {
                Timber.tag(tag).d(tr, "Exception")
            }
        }
    }

    fun debug(tag: String, msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).d(tr, "$msg")
            } else {
                Timber.tag(tag).d(tr, "Exception")
            }
        }
    }

    fun info(msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).i("$msg")
            }
        }
    }

    fun info(tag: String, msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).i("$msg")
            }
        }
    }

    fun info(msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).i(tr, "$msg")
            } else {
                Timber.tag(tag).i(tr, "Exception")
            }
        }
    }

    fun info(tag: String, msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).i(tr, "$msg")
            } else {
                Timber.tag(tag).i(tr, "Exception")
            }
        }
    }

    fun warn(msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).w("$msg")
            }
        }
    }

    fun warn(tag: String, msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).w("$msg")
            }
        }
    }

    fun warn(tag: String, msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).w(tr, "$msg")
            } else {
                Timber.tag(tag).w(tr, "Exception")
            }
        }
    }

    fun error(msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).e("$msg")
            }
        }
    }

    fun error(tag: String, msg: Any?) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).e("$msg")
            }
        }
    }

    fun error(msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).e(tr, "$msg")
            } else {
                Timber.tag(tag).e(tr, "Exception")
            }
        }
    }

    fun error(tag: String, msg: Any?, tr: Throwable) {
        if (isDebug) {
            if (msg != null) {
                Timber.tag(tag).e(tr, "$msg")
            } else {
                Timber.tag(tag).e(tr, "Exception")
            }
        }
    }

    companion object {

        private var isDebug = false

        fun init(isDebug: Boolean) {
            Companion.isDebug = isDebug
        }

        fun getLogger(clazz: Class<*>): Logger {
            return Logger(clazz)
        }
    }*/
}