package com.taywin.satfpayslip.ui.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavGraph
import androidx.navigation.NavGraphNavigator
import androidx.navigation.findNavController
import com.taywin.satfpayslip.R
import com.taywin.satfpayslip.ui.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpdateView()
    }

    private fun setUpdateView() {
//        TODO("Not yet implemented")
        txtRegister.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToRegisterFragment()
            it.findNavController().navigate(action)
        }

        btnLogin.setOnClickListener {
            val action = MainFragmentDirections.actionMainFragmentToDetailSlipMonnyFragment()
            it.findNavController().navigate(action)
        }
    }

}