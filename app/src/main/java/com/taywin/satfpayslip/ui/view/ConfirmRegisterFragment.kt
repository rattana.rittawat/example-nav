package com.taywin.satfpayslip.ui.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taywin.satfpayslip.R
import com.taywin.satfpayslip.ui.viewmodel.ConfirmRegisterViewModel

class ConfirmRegisterFragment : Fragment() {

    companion object {
        fun newInstance() = ConfirmRegisterFragment()
    }

    private lateinit var viewModel: ConfirmRegisterViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.confirm_register_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ConfirmRegisterViewModel::class.java)
        // TODO: Use the ViewModel
    }

}