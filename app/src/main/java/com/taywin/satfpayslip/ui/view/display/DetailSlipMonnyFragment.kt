package com.taywin.satfpayslip.ui.view.display

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.taywin.satfpayslip.R
import com.taywin.satfpayslip.ui.viewmodel.DetailSlipMonnyViewModel
import com.taywin.satfpayslip.util.AlertDialogUtils
import kotlinx.android.synthetic.main.detail_slip_monny_fragment.*

class DetailSlipMonnyFragment : Fragment() {

    companion object {
        fun newInstance() = DetailSlipMonnyFragment()
    }

    private lateinit var viewModel: DetailSlipMonnyViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_slip_monny_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailSlipMonnyViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpdateView()
    }

    private fun setUpdateView() {
//        TODO("Not yet implemented")
        btnEditPass.setOnClickListener {
            AlertDialogUtils.ShowDialogDateTimer(
                    requireContext(),
                    null,
                    "",
                    "",
                    "",
                    null
            )
        }
    }

}