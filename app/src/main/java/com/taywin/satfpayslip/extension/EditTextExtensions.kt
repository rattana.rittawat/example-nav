package com.taywin.satfpayslip.extension

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat

fun AppCompatEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

fun AppCompatEditText.onTextChanged(onTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged.invoke((s.toString()))
        }
    })
}

/**
 * Add [TextWatcher] for edit text and provides result of [TextWatcher.beforeTextChanged], [TextWatcher.afterTextChanged],
 * [TextWatcher.onTextChanged] through the given functions.
 * ***Note*** that each calling of this method would create and add new instance of [TextWatcher]
 *
 * @return created [TextWatcher]
 */
fun EditText.addTextChangedListener(
    onTextChanged: ((text: CharSequence?, start: Int, before: Int, count: Int) -> Unit)? = null,
    afterTextChanged: ((editable: Editable?) -> Unit)? = null,
    beforeTextChanged: ((s: CharSequence?, start: Int, count: Int, after: Int) -> Unit)? = null
): TextWatcher {
    val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged?.invoke(s)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            beforeTextChanged?.invoke(s, start, count, after)
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            onTextChanged?.invoke(s, start, before, count)
        }
    }
    addTextChangedListener(watcher)
    return watcher
}

/*
fun TextView.textColorFocused() =
    setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))

fun TextView.textColorNormal() =
    setTextColor(ContextCompat.getColor(context, R.color.colorPrussianBlue))

fun TextView.textColorError() =
    setTextColor(ContextCompat.getColor(context, R.color.colorRed))
*/

