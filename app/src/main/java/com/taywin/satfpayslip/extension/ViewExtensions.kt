package com.taywin.satfpayslip.extension

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

//update text color
fun TextView.updateTextColor(@ColorRes idRes: Int) {
    this.setTextColor(
        ContextCompat.getColor(
            context!!,
            idRes
        ))
}

fun Button.updateButtonColor(@ColorRes idRes: Int) {
    this.setTextColor(
        ContextCompat.getColor(
            context!!,
            idRes
        ))
    this.setBackgroundColor(
        ContextCompat.getColor(context!!, idRes)
    )
}

fun View.clickWithDebounce(debounceTime: Long = 600L, action: (view: View) -> Unit): Disposable =
    RxView.clicks(this)
        .throttleFirst(debounceTime, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
        .subscribe { action(this) }

@SuppressLint("ClickableViewAccessibility")
fun View.setOnAnimateClickListener(resId: Int?, onClick: (View) -> Unit) {
    var secondaryView: View? = null

    if (resId != null) {
        secondaryView = this.findViewById(resId)
    }

    this.clickWithDebounce { view ->
        Handler().postDelayed(
            {
                onClick.invoke(view)
            },
            250
        )
    }

    this.setOnTouchListener { v, event ->

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                v.animate().cancel()
                v.animate().scaleY(0.96f).scaleX(0.96f).setDuration(200).start()
                secondaryView?.animate()?.scaleY(1.1f)?.scaleX(1.1f)?.alpha(0.7f)?.setDuration(200)
                    ?.start()

                false
            }
            MotionEvent.ACTION_UP -> {
                val xBigScale = ObjectAnimator.ofFloat(v, "scaleX", 1.03f)
                xBigScale.setDuration(160).repeatCount = 0

                val yBigScale = ObjectAnimator.ofFloat(v, "scaleY", 1.03f)
                yBigScale.setDuration(160).repeatCount = 0

                val xSmallScale = ObjectAnimator.ofFloat(v, "scaleX", 0.985f)
                xSmallScale.setDuration(140).repeatCount = 0

                val ySmallScale = ObjectAnimator.ofFloat(v, "scaleY", 0.985f)
                ySmallScale.setDuration(140).repeatCount = 0

                val xNormalScale = ObjectAnimator.ofFloat(v, "scaleX", 1f)
                xNormalScale.setDuration(70).repeatCount = 0

                val yNormalScale = ObjectAnimator.ofFloat(v, "scaleY", 1f)
                yNormalScale.setDuration(70).repeatCount = 0

                val animateSet = AnimatorSet()
                animateSet.play(xBigScale).with(yBigScale)
                animateSet.play(xSmallScale).after(xBigScale)
                animateSet.play(ySmallScale).after(yBigScale)
                animateSet.play(xNormalScale).after(xSmallScale)
                animateSet.play(yNormalScale).after(ySmallScale)
                animateSet.start()

                secondaryView?.animate()?.scaleY(1f)?.scaleX(1f)?.alpha(1f)?.setDuration(370)
                    ?.start()

                false
            }

            MotionEvent.ACTION_CANCEL -> {
                v.animate().scaleY(1f).scaleX(1f).setDuration(200).start()
                secondaryView?.animate()?.scaleY(1f)?.scaleX(1f)?.alpha(1f)?.setDuration(370)
                    ?.start()

                true
            }

            else -> true
        }
    }
}

fun View.updateBackground(@DrawableRes idRes: Int) {
    this.background = ContextCompat.getDrawable(
        context,
        idRes
    )
}

fun View.setOnAnimateClickListener(onClick: (View) -> Unit) {
    this.setOnAnimateClickListener(null, onClick)
}

/** Set the View visibility to VISIBLE and eventually animate the View alpha till 100% */
fun View.visible(animate: Boolean = true,duration: Long = 300) {
    if (animate) {
        animate().alpha(1f).setDuration(duration).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                visibility = View.VISIBLE
            }
        })
    } else {
        visibility = View.VISIBLE
    }
}

/** Set the View visibility to INVISIBLE and eventually animate view alpha till 0% */
fun View.invisible(animate: Boolean = true) {
    hide(View.INVISIBLE, animate)
}

/** Set the View visibility to GONE and eventually animate view alpha till 0% */
fun View.gone(animate: Boolean = true,duration: Long = 300) {
    hide(View.GONE, animate,duration)
}

/** Convenient method that chooses between View.visible() or View.invisible() methods */
fun View.visibleOrInvisible(show: Boolean, animate: Boolean = true) {
    if (show) visible(animate) else invisible(animate)
}

/** Convenient method that chooses between View.visible() or View.gone() methods */
fun View.visibleOrGone(show: Boolean, animate: Boolean = true) {
    if (show) visible(animate) else gone(animate)
}

fun View.hide(hidingStrategy: Int, animate: Boolean = true,duration: Long = 300) {
    if (animate) {
        animate().alpha(0f).setDuration(duration).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                visibility = hidingStrategy
            }
        })
    } else {
        visibility = hidingStrategy
    }
}

fun AlertDialog.setFullWidth(){
    val lp = WindowManager.LayoutParams()
    lp.copyFrom(this.window?.attributes)
    lp.width = WindowManager.LayoutParams.MATCH_PARENT
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    this.window?.attributes = lp
}