package com.taywin.satfpayslip

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.jakewharton.threetenabp.AndroidThreeTen
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.orhanobut.logger.PrettyFormatStrategy
import okhttp3.OkHttpClient
import timber.log.Timber
import java.util.concurrent.TimeUnit

class BaseApplication : Application() {

    @SuppressLint("StaticFieldLeak")
    object C {
        lateinit var mContext: Context
    }

    override fun onCreate() {
        super.onCreate()

        /*startKoin {
            androidLogger()
            androidContext(this@CountiesApplication)
            modules(
                apiModule,
                viewModelModule,
                repositoryModule,
                networkModule,
                databaseModule
            )
        }*/

       // Logger.init(BuildConfig.DEBUG)
        AndroidThreeTen.init(this)
        with(
            OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build()
        ) {
            AndroidNetworking.initialize(this@BaseApplication, this)
        }

        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)
        }
        PrettyFormatStrategy.newBuilder()
            .showThreadInfo(false) // (Optional) Whether to show thread info or not. Default true
            .methodCount(0) // (Optional) How many method line to show. Default 2
            .methodOffset(7) // (Optional) Hides internal method calls up to offset. Default 5
//                .logStrategy(customLog) // (Optional) Changes the log strategy to print out. Default LogCat
            .tag("custom") // (Optional) Global tag for every log. Default PRETTY_LOGGER
            .build().also {
                com.orhanobut.logger.Logger.addLogAdapter(AndroidLogAdapter(it))
            }
        Timber.plant(
            if (BuildConfig.DEBUG)
//            Timber.DebugTree()
                object : Timber.DebugTree() {
                    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
                        com.orhanobut.logger.Logger.log(priority, tag, message, t)
                    }
                }
            else
                CrashReportingTree()
        )

    }

    private class CrashReportingTree : Timber.Tree() {

        override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }
            val t = throwable ?: Exception(message)
//            Crashlytics.log(priority, tag, message)
//            Crashlytics.logException(t)
        }
    }

    companion object {
        fun getContext(): Context {
            return C.mContext
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        //MultiDex.install(this)
    }

}